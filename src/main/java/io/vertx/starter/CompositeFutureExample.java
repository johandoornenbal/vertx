package io.vertx.starter;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

public class CompositeFutureExample extends AbstractVerticle {

  Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  private Future<String> action1(){
    Future<String> future = Future.future();
    vertx.setTimer(200, l->{
      logger.debug("completing action 1");
      future.complete("hi from action1");
    });
    return future;
  }

  private Future<String> action2(){
    Future<String> future = Future.future();
    vertx.setTimer(200, l->{
      logger.debug("completing action 2");
      future.complete("hi from action2");
    });
    return future;
  }

  private Future<String> action3(){
    Future<String> future = Future.future();
    vertx.setTimer(200, l->{
      logger.debug("completing action 3");
      future.complete("hi from action3");
    });
    return future;
  }

  private Future<String> combine123(){
    Future<String> future = Future.future();
    Future<String> f1 = action1(); // by storing the future, composite future works....
    Future<String> f2 = action2();
    Future<String> f3 = action3();
    CompositeFuture
      .join(Arrays.asList(f1, f2, f3))
      .setHandler(ar->{
        if (ar.succeeded()){
          logger.debug("f1 complete: " + String.valueOf(f1.isComplete()));
          logger.debug("f2 complete: " + String.valueOf(f2.isComplete()));
          logger.debug("f3 complete: " + String.valueOf(f3.isComplete()));
          logger.debug("hi from handler within 'combine future': " + f1.result() + " " + f2.result() + " " + f3.result());
          future.complete(f1.result() + " " + f2.result() + " " + f3.result());
        } else {
          logger.debug("combine failed");
        }
      });
    return future;
  }


  @Override
  public void start() {
    logger.debug("Starting verticle");
    combine123().setHandler(
      ar->{
        if (ar.succeeded()) {
          logger.debug("hi from handler when succeeded 'combine future': " + ar.result());
        } else {
          logger.debug("failing ....");
        }
      });
  }

  @Override
  public void stop(){
    logger.debug("Stopping verticle");

  }

}
