package io.vertx.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

public class MainVerticle extends AbstractVerticle {

  Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  @Override
  public void start() {
    logger.debug("Starting verticle");
    startExample("CompositeFutureExample");
    startExample("CompositeFutureWithEnumExample");
  }

  private void startExample(final String s) {
    String str1 = "io.vertx.starter." + s;
    String str2 = s + " deployed ok, deploymentID = ";
    DeploymentOptions options = new DeploymentOptions().setInstances(4);
    vertx.deployVerticle(str1, options, res -> {
      if (res.succeeded()) {
        String deploymentID = res.result();
        logger.debug(str2 + deploymentID);
      } else {
        res.cause().printStackTrace();
      }
    });
  }

  @Override
  public void stop(){
    logger.debug("Stopping verticle");
  }

  // Convenience method so you can run it in your IDE
  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new MainVerticle());
  }

}
