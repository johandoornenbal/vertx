package io.vertx.starter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class CompositeFutureWithEnumExample extends AbstractVerticle {

  enum SomeEnum {

    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5");

    private final String name;

    SomeEnum(final String name) {
      this.name = name;
    }

    Future<String> action(Vertx vertx, Logger logger){
      Future<String> future = Future.future();
      vertx.setTimer(3000, l->{               // just wait a little longer to build some tension ;-)
        logger.debug("completing action " + name);
        future.complete("hi from action " + name);
      });
      return future;
    }

  }

  Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  private Future<String> combine123(){
    Future<String> future = Future.future();
    List<Future> futures = new ArrayList<>();
    for (SomeEnum e : SomeEnum.values()){
      futures.add(e.action(vertx, logger));
    }
    CompositeFuture
      .join(futures)
      .setHandler(ar->{
        if (ar.succeeded()){
          final StringBuilder builder = new StringBuilder();
          futures.forEach(
            f->{
              logger.debug("complete: " + String.valueOf(f.isComplete()));
              builder.append(f.result());
              builder.append(" ");
            }
          );
          logger.debug("hi from handler within 'combine future': " + builder.toString());
          future.complete(builder.toString());
        } else {
          logger.debug("combine failed");
        }
      });
    return future;
  }


  @Override
  public void start() {
    logger.debug("Starting verticle");
    combine123().setHandler(
      ar->{
        if (ar.succeeded()) {
          logger.debug("hi from handler when succeeded 'combine future': " + ar.result());
        } else {
          logger.debug("failing ....");
        }
      });
  }

  @Override
  public void stop(){
    logger.debug("Stopping verticle");

  }

}
